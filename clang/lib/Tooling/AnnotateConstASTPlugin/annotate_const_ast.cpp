#include "clang/AST/ASTConsumer.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/AST/Attr.h"
#include "clang/AST/Attrs.inc"
#include "clang/AST/DeclVisitor.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/FrontendAction.h"
#include "clang/Tooling/Tooling.h"
#include "clang/Frontend/FrontendPluginRegistry.h"

#include <vector>
#include <unordered_map>

const std::string constAnnotationValue = "constAnnotationValue";
const std::string constAnnotationPointer = "constAnnotationPointer";

using namespace clang;

class AnnotateClassVisitor : public RecursiveASTVisitor<AnnotateClassVisitor> {
public:
	explicit AnnotateClassVisitor(ASTContext *Context) : Context(Context) {}

	bool VisitValueDecl(ValueDecl *Declaration) {
		if (dyn_cast<ParmVarDecl>(Declaration))		// empêche l'annotation des arguments de fonctions
			return true;

		static std::unordered_map<int, AnnotateAttr*> annot_map;

		std::vector<bool> vect;

		QualType q = Declaration->getType();
		while (q->isPointerType()) {
			vect.push_back(q.isConstQualified());
			q = q->getPointeeType();
		}
		vect.push_back(q.isConstQualified());

		unsigned i = 0;
		for (auto it = vect.rbegin(); it != vect.rend(); it++) {
			if (*it) {
				if (annot_map.find(i) != annot_map.end()) {
					Declaration->addAttr(annot_map[i]);
				}
				else {
					if (!i)
						annot_map[i] = AnnotateAttr::Create(*Context, constAnnotationValue, {SourceRange{}});
					else
						annot_map[i] = AnnotateAttr::Create(*Context, constAnnotationPointer+std::to_string(i), {SourceRange{}});
					Declaration->addAttr(annot_map[i]);
				}
			}
			i++;
		}
		return true;
	}

private:
	ASTContext *Context;
};

class AnnotateClassConsumer : public clang::ASTConsumer {
public:
	explicit AnnotateClassConsumer(ASTContext *Context) : Visitor(Context) {}

	virtual void HandleTranslationUnit(clang::ASTContext &Context) override {
		Visitor.TraverseDecl(Context.getTranslationUnitDecl());
	}

private:
	AnnotateClassVisitor Visitor;
};

class AnnotateClassAction : public clang::PluginASTAction {
public:
	bool ParseArgs(const CompilerInstance &CI, const std::vector<std::string> &args) override {return true;}

	virtual std::unique_ptr<clang::ASTConsumer> CreateASTConsumer(clang::CompilerInstance &Compiler, llvm::StringRef InFile) override {
		return std::make_unique<AnnotateClassConsumer>(&Compiler.getASTContext());
	}

	// Automatically run the plugin before the main AST action
	PluginASTAction::ActionType getActionType() override {
		return AddBeforeMainAction;
	}
};

static FrontendPluginRegistry::Add<AnnotateClassAction>
X("annotate_const", "Add annotation on declaration for the constant levels (const int * *const is 0, 2)");