# Documentation annotate_const_ast

Cet outil s'utilise avec les autres outils de même objectif : `my_ir_editor` et `x86editor`.  
Il est conçu sur la base de LLVM 12.0.0.  

# Comportement

Ajoute des attributs annotation aux déclarations constantes afin de transférer l'information à l'IR.  
Ces annotations sont multiples de sorte à avoir une information détaillée (on est capable de différentier `const int *const*` et `const int **const` par exemple).  

# Utilisation

L'outil a été pensé comme un plugin.  

Pour s'en servir, il est nécessaire de charger le fichier objet partagé et d'activer le plugin.

Le plugin est chargé avec `-Xclang -load -Xclang [chemin où à été compilé LLVM]/lib/annotate_const_ast.so `  
Et activé avec `-Xclang -add-plugin -Xclang annotate_const`  
La modification de l'AST n'étant pas transférée jusqu'à la compilation, il faut produire un fichier AST intermédiaire. Pour cela on rajoute l'option `-emit-ast`  
Ce qui donne comme commande finale `llvm-project/build/bin/clang -S -emit-llvm -Xclang -load -Xclang llvm-project/build/lib/annotate_const_ast.so -Xclang -add-plugin -Xclang annotate_const -emit-ast file.c -o out.ast`  

# Algorithme Principal de Fonctionnement

- Parcours des ValueDecl (déclaration de variables, fonctions, champs de structure, ...).
	- Abandon pour les ParmVarDecl (l'argument du profil de la fonction)
	- Déduction des niveaux de constantes (const int * = 0; int *const = 1; const int *const = 0,1).
	- Ajout de l'information sous forme d'annotations.

# Modifications apportées à LLVM

L'outil à été intégré à l'architecture LLVM, les sources se trouvent sous :  
- Nouveau Dossier : `llvm-project/clang/lib/Tooling/AnnotateConstASTPlugin/`
- Code source : `llvm-project/clang/lib/Tooling/AnnotateConstASTPlugin/annotate_const_ast.cpp`  
- CMakeLists : `llvm-project/clang/lib/Tooling/AnnotateConstASTPlugin/CMakeLists.txt`

Les fichiers modifiés pour l'intégration et leur modification sont :
- `llvm-project/clang/lib/Tooling/CMakeLists.txt`
	```
	add_subdirectory(AnnotateConstASTPlugin)
	```