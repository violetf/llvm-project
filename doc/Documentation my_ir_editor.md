# Documentation my_ir_editor

Cet outil s'utilise avec les autres outils de même objectif : `annotate_const_ast` et `x86editor`.  
Il est conçu sur la base de LLVM 12.0.0.  

# Comportement

Utilise les annotations ajoutées lors de la création de l'IR (llvm.xxx.annotations) afin d'analyser les constantes associées avec la chaîne mise lors de la modification de l'AST.  
Après analyse, modifie l'IR en ajoutant une metadata annotation correspondant au type de constante aux instructions IR afin de transmettre l'information à la génération de code.  
Crée des doublons de fonctions et modifie la référence d'appel pour chaque type de profil, pour pouvoir implanter les bons compteurs.  

# Utilisation

L'outil à été pensé comme une passe d'optimisation de l'IR et peut être utilisé depuis plusieurs points de départ. 

À partir de clang, il suffit d'ajouter les options `-mllvm -my_ir_editor`.  
ex : `clang -mllvm -my_ir_editor file.c` compile le programme file.c en annotant l'IR (sans effet notable sans les outils annotate_const_ast et x86editor)  
`clang -S -emit-llvm -mllvm -my_ir_editor file.ast -o out.ll` permet d'annoter l'IR et d'en produire le fichier à partir d'un fichier AST (comme la sortie de l'outil annotate_const_ast par exemple)

À partir de opt, l'option `-passes=my_ir_editor` permet de faire fonctionner la passe seule.  
ex : `opt -passes=my_ir_editor file.ll -o out.ll` applique la transformation sur le fichier file.ll et produit out.ll.

# Algorithme Principal de Fonctionnement

- Récupère les variables globales annotées

- Crée une liste initiale de fonctions à modifier (initialisé avec toutes les fonctions du module) et initialise la liste de référence
	`map<FunctionProfileTy, Function*>` qui prend en clé un profil (la fonction de base et les caractéristiques des arguments d'appel) et donne la fonction correspondante

- Crée les clones des fonctions de base (Après avoir été modifiée, faire un clone copie les modifications, on fait donc une copie saine préalable qui servira de base de copie)

- Tant que la liste de fonctions à modifier n'est pas vide :
	- on prend un élément et on le supprime de la liste
	- on fait une analyse vers le haut des constantes depuis les annotations (une nouvelle à chaque fonction pour avoir les bonnes références, puisque le fonctionnement est par adresse, une analyse unique pour la fonction de base donnerait les référence vers elle uniquement et non vers les clones)
	- on propage les constantes vers le bas et on ajoute les nouvelles fonctions (profil non-vu) à la liste après avoir fait un clone et on modifie la référence de l'appel

# Détails de traitement des fonctions

Afin de pouvoir incrémenter les bons compteurs, il est essentiel de distinguer le caractère des arguments. Une fonction indiquant en argument un pointeur vers un entier constant et faisant une lecture de ce dernier peut être appelée avec un entier non-constant. Dans ce cas ci, il sera compté comme un accès à une donnée constante alors que ce n'est pas le cas. C'est pourquoi un duplicata de fonction est créé pour chaque profil distinct de fonction.  
Le profil `FunctionProfileTy` est basé sur l'adresse de la fonction de base, et la liste précise des caractères constants des arguments (`const int **` est différent de `const int *const*`)  
Les appels indirects ne sont pas traités, comme il est difficile voir impossible de retrouver à la compilation la fonction appelée.

# Détails de propagation des constantes

- GetElementPtr/ExtractElement/ExtractValue :
	- La propagation vers le haut donne les caractéristiques à l'instruction, mais ne se propage pas plus haut. Lors d'une propagation vers le haut, c'est le sous élément de la structure qui est annoté `const`, on ne veut donc pas transmettre le caractère à la structure.
	- La propagation vers le bas transmet au membre le caractère de son type (ex : un membre `int*` se voit propagé `int*const`) seulement si la structure est constante par valeur (ex : un pointeur constant sur structure non-constante ne doit pas propager).

- Load/Store :
	- La propagation n'est effectuée que s'il s'agit d'un accès `<ty*> <ty**>` (<ty> pouvant être n'importe quel type). En effet, la copie de la valeur d'une constante ne propage pas sa caractéristique.

- Bitcast :
	- La propagation est la même dans les deux sens, on propage tout le temps les caractéristiques, bitcast étant une conversion de type propre à LLVM, No-op, sans changement des bits.

- Call :
	- Seulement pour la propagation vers le haut, on ajoute l'instruction aux constantes et on arrête la propagation (dead end)

- Phi :
	- Propagation vers le haut : on ajoute l'instruction aux constantes et on continue la propagation pour tous les aliases qui sont des instructions ou des arguments
	- Propagation vers le bas : Si l'un des éléments a un caractère constant, on le propage aux autres membres

# Modifications apportées à LLVM

L'outil à été intégré à l'architecture LLVM, les sources se trouvent sous :  
- Code source : `llvm-project/llvm/lib/Transforms/Utils/my_ir_editor.cpp`  
- Header : `llvm-project/llvm/include/llvm/Transforms/Utils/my_ir_editor.h`  

Les fichiers modifiés pour l'intégration et leur modification sont :
- `llvm-project/llvm/lib/Transforms/Utils/CMakeLists.txt`
	```
	my_ir_editor.cpp	# inside add_llvm_component_library(LLVMTransformUtils
	```

- `llvm-project/llvm/lib/Passes/PassRegistry.def`
	```
	MODULE_PASS("my_ir_editor", MyIREditorPass())
	```

- `llvm-project/llvm/lib/Passes/PassBuilder.cpp`
	```cpp
	#include "llvm/Transforms/Utils/my_ir_editor.h"
	```

- `llvm-project/clang/lib/CodeGen/BackendUtil.cpp`  
	- en entête de fichier :
	```cpp
	#include "llvm/Transforms/Utils/my_ir_editor.h"
	```
	- en entête après `using namespace llvm;`
	```cpp
	cl::opt<bool> EnableMyIREditorPass("my_ir_editor", cl::desc("Analyse 'constAnnotation' annotations then add metadata annotation for MachineIR modifications."), cl::init(false), cl::Hidden);
	```
	- dans la fonction `EmitAssemblyHelper::EmitAssemblyWithNewPassManager`, après la création de `PassBuilder PB`
	```cpp
	if (EnableMyIREditorPass)  
		PB.registerOptimizerLastEPCallback([&](ModulePassManager &MPM, PassBuilder::OptimizationLevel Level) { MPM.addPass(MyIREditorPass()); });
	```