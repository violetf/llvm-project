# Documentation x86editor

Cet outil s'utilise avec les autres outils de même objectif : `annotate_const_ast` et `my_ir_editor`.  
Il est conçu sur la base de LLVM 12.0.0.  

# Comportement

Utilise les annotations ajoutées lors de la modification de l'IR (metadata ajoutée aux instructions) pour placer les bons compteurs lors d'accès mémoire.  
Ajoute également des compteurs d'accès mémoire pour les effets de bord (push/pop/call).  

# Utilisation

L'outil à été pensé comme une passe sur la MachineIR et peut être utilisé depuis plusieurs points de départ. 

À partir de llc, l'option `-x86editor` permet de faire fonctionner la passe.  
ex : `llc -x86editor file.ll -o out.s` applique la transformation sur le fichier file.ll et produit out.s.  

À partir de clang, il faut ajouter les options `-mllvm -x86editor` et le fichier objet `cpt_manager.o` qui contient le code pour les compteurs et l'affichage.  
ex : `clang -mllvm -x86editor file.c cpt_manager.o` compile le programme file.c en ajoutant les compteurs (sans les autres outils compte les accès mémoire sans distinction de constante)  
`clang -mllvm -my_ir_editor -mllvm -x86editor file.ast cpt_manager.o -o out` compile le programme depuis le fichier AST, avec annotation de l'IR et ajout des compteurs

# Algorithme Principal de Fonctionnement

- On parcourt les blocs de base.
	- On parcourt les instructions machine.
		- Si la liste des opérandes mémoire n'est pas vide, alors c'est un accès mémoire.
			- On regarde si c'est un Load ou un Store.
			- Dans cette liste, pour les opérandes on regarde l'annotation, et si c'est la bonne on regarde les données qu'elle inclut, sinon on continue.
			- Si c'est la bonne annotation, on compare avec le type de l'accès en cours, on ajoute le compteur correspondant et on passe à la suite.
			- Si au bout de la liste on n'a pas trouvé l'annotation, ce n'est pas une constante et on ajoute le compteur correspondant.
		- Même sans opérande mémoire, il peut y avoir des accès mémoire selon l'infrastructure. On ajoute les compteurs correspondant selon l'instruction rencontrée.
		- On enregistre l'emplacement si on croise un point de sortie. 
	- On place le code d'incrémentation des compteurs.
	- On ajoute l'affichage au point de sortie enregistré.

# Modifications apportées à LLVM

L'outil à été intégré à l'architecture LLVM, les sources se trouvent sous :  
- Code source : `llvm-project/llvm/lib/Target/X86/my_mir_editor.cpp`  

Les fichiers modifiés pour l'intégration et leur modification sont :
- `llvm-project/llvm/lib/Target/X86/CMakeLists.txt`
	```
	my_mir_editor.cpp	# inside set(sources
	```

- `llvm-project/llvm/lib/Target/X86/X86.h`
	```cpp
	FunctionPass *createMyMIRPassPass();
	```
	```cpp
	void initializeMyMIRPassPass(PassRegistry &);
	```

- `llvm-project/llvm/lib/Target/X86/X86TargetMachine.cpp`
	- En entête de fichier :
	```cpp
	cl::opt<bool> EnableMyMIRPass("x86editor", cl::desc("Add memory access counters for constants and not-constants using IR metadata annotations."), cl::init(false), cl::Hidden);
	```
	- Dans la fonction `X86PassConfig::addPreEmitPass2()` :
	```cpp
	if (EnableMyMIRPass)
		addPass(createMyMIRPassPass());
	```
