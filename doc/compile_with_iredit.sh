#!/usr/bin/bash

# 30-37 = foreground // add+10 40-47 for the background color
# \e is GNU only -> ISO is \033
noir='\e[0;30m'
gris='\e[1;30m'
rougefonce='\e[0;31m'
rougeclair='\e[1;31m'
vertfonce='\e[0;32m'
vertclair='\e[1;32m'
orange='\e[0;33m'
jaune='\e[1;33m'
bleufonce='\e[0;34m'
bleuclair='\e[1;34m'
violetfonce='\e[0;35m'
violetclair='\e[1;35m'
cyanfonce='\e[0;36m'
cyanclair='\e[1;36m'
grisclair='\e[0;37m'
blanc='\e[1;37m'

# reset foregroung & background
neutre='\e[0;m'

function clean() {
	rm -f ${astfiles} ${llfiles} ${fused_file}.ll ${fused_file}.annot.ll ${fused_file}.s
}

# path to the binaries
bin='../llvm-project/build/bin'
plugin_path='../llvm-project/build/lib/annotate_const_ast.so'

# name of custom command line options
plugin_name='annotate_const'
ir_opt_name='my_ir_editor'
ir_pass_name='my_ir_editor'
mir_opt_name='x86editor'

# list of files names with no extension and .ast and .ll extension
filenames=''
astfiles=''
llfiles=''
#temp_llfiles=''

fused_file='global_ir'
#fused_file='/tmp/global_ir'

for var in "$@"
do
	echo ""
	name=$(echo "${var%.*}")
	#name=`basename "$(echo "${var%.*}")"`		pour ensuite mettre les tmp file dans /tmp
	extension=$(echo "${var##*.}")
	filenames="${filenames} `basename "${name}"`"
	astfiles="${astfiles} ${name}.ast"
	llfiles="${llfiles} ${name}.ll"
	#temp_llfiles="${temp_llfiles} /tmp/`basename "${name}"`${name}.ll"
	echo "Fichier ${var} :"

# --------------------------------------------------------------------------------------------------------
# Annotation du fichier source par modification de l'AST
	if [ "${extension}" = "c" ]
	then
		${bin}/clang -S -emit-llvm -Xclang -load -Xclang ${plugin_path} -Xclang -add-plugin -Xclang ${plugin_name} -emit-ast "${var}" -o "${name}.ast"
		if [ $? -ne 0 ]; then
			echo -e "\tAnnotation AST\tSrc -> AST \t ${rougeclair}ERREUR${neutre}"
			exit 1
		else
			echo -e "\tAnnotation AST\tSrc -> AST \t ${vertclair}OK${neutre}"
		fi
	elif [ "${extension}" = "cpp" ]
	then
		${bin}/clang++ -S -emit-llvm -Xclang -load -Xclang ${plugin_path} -Xclang -add-plugin -Xclang ${plugin_name} -emit-ast "${var}" -o "${name}.ast"
		if [ $? -ne 0 ]; then
			echo -e "\tAnnotation AST\tSrc -> AST \t ${rougeclair}ERREUR${neutre}"
			exit 1
		else
			echo -e "\tAnnotation AST\tSrc -> AST \t ${vertclair}OK${neutre}"
		fi
	else
		echo -e "\tWorks on '.c' or '.cpp' files only"
		exit 2
	fi

# --------------------------------------------------------------------------------------------------------
# Obtention de la Représentation Intermédiaire
	if [ "${extension}" = "c" ]
	then
		${bin}/clang -O3 -S -emit-llvm "${name}.ast" -o "${name}.ll"
		if [ $? -ne 0 ]; then
			echo -e "\tIntermédiaire\tAST -> IR \t ${rougeclair}ERREUR${neutre}"
			exit 2
		else
			echo -e "\tIntermédiaire\tAST -> IR \t ${vertclair}OK${neutre}"
		fi
	elif [ "${extension}" = "cpp" ]
	then
		${bin}/clang++ -O3 -S -emit-llvm "${name}.ast" -o "${name}.ll"
		if [ $? -ne 0 ]; then
			echo -e "\tIntermédiaire\tAST -> IR \t ${rougeclair}ERREUR${neutre}"
			exit 2
		else
			echo -e "\tIntermédiaire\tAST -> IR \t ${vertclair}OK${neutre}"
		fi
	else
		echo -e "\tWorks on '.c' or '.cpp' files only"
		exit 2
	fi

done

echo ""
echo "Fusion des fichiers :"



# --------------------------------------------------------------------------------------------------------
# unification des fichiers IR .ll
${bin}/llvm-link -S ${llfiles} -o ${fused_file}.ll
if [ $? -ne 0 ]; then
	echo -e "\tFusion	\tIR -> IR \t ${rougeclair}ERREUR${neutre}"
	exit 3
else
	echo -e "\tFusion	\tIR -> IR \t ${vertclair}OK${neutre}"
fi

# --------------------------------------------------------------------------------------------------------
# Annotation de l'IR
${bin}/opt -S -passes=${ir_pass_name} ${fused_file}.ll -o ${fused_file}.annot.ll
if [ $? -ne 0 ]; then
	echo -e "\tAnnotation IR\tIR -> IR \t ${rougeclair}ERREUR${neutre}"
	exit 4
else
	echo -e "\tAnnotation IR\tIR -> IR \t ${vertclair}OK${neutre}"
fi

# --------------------------------------------------------------------------------------------------------
# Passage en assembleur IR -> assembleur
${bin}/llc -${mir_opt_name} ${fused_file}.annot.ll -o ${fused_file}.s
if [ $? -ne 0 ]; then
	echo -e "\tAssembleur\tIR -> S \t ${rougeclair}ERREUR${neutre}"
	exit 5
else
	echo -e "\tAssembleur\tIR -> S \t ${vertclair}OK${neutre}"
fi

# --------------------------------------------------------------------------------------------------------
# Compilation assembleur -> machine
${bin}/clang++ cpt_manager.s ${fused_file}.s -o out.exe
if [ $? -ne 0 ]; then
	echo -e "\tExécutable\tS -> exe \t ${rougeclair}ERREUR${neutre}"
	exit 6
else
	echo -e "\tExécutable\tS -> exe \t ${vertclair}OK${neutre}"
fi

# --------------------------------------------------------------------------------------------------------
# Nettoyage des fichiers temporaires
#clean