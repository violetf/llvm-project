	.text
	.file	"cpt_manager.c"
	.globl	print_cpt                       # -- Begin function print_cpt
	.p2align	4, 0x90
	.type	print_cpt,@function
print_cpt:                              # @print_cpt
	.cfi_startproc
# %bb.0:
	pushq	%rax
	.cfi_def_cfa_offset 16
	movq	cpt_store_const(%rip), %rsi
	movq	cpt_store_tt(%rip), %rcx
	addq	%rsi, %rcx
	movq	%rcx, cpt_store_tt(%rip)
	movq	cpt_load_const(%rip), %rdx
	movq	cpt_load_tt(%rip), %r8
	addq	%rdx, %r8
	movq	%r8, cpt_load_tt(%rip)
	movq	cpt_push(%rip), %r9
	movq	cpt_pop(%rip), %rax
	movq	%rax, (%rsp)
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	popq	%rax
	.cfi_def_cfa_offset 8
	retq
.Lfunc_end0:
	.size	print_cpt, .Lfunc_end0-print_cpt
	.cfi_endproc
                                        # -- End function
	.type	cpt_store_const,@object         # @cpt_store_const
	.bss
	.globl	cpt_store_const
	.p2align	3
cpt_store_const:
	.quad	0                               # 0x0
	.size	cpt_store_const, 8

	.type	cpt_load_const,@object          # @cpt_load_const
	.globl	cpt_load_const
	.p2align	3
cpt_load_const:
	.quad	0                               # 0x0
	.size	cpt_load_const, 8

	.type	cpt_store_tt,@object            # @cpt_store_tt
	.globl	cpt_store_tt
	.p2align	3
cpt_store_tt:
	.quad	0                               # 0x0
	.size	cpt_store_tt, 8

	.type	cpt_load_tt,@object             # @cpt_load_tt
	.globl	cpt_load_tt
	.p2align	3
cpt_load_tt:
	.quad	0                               # 0x0
	.size	cpt_load_tt, 8

	.type	cpt_push,@object                # @cpt_push
	.globl	cpt_push
	.p2align	3
cpt_push:
	.quad	0                               # 0x0
	.size	cpt_push, 8

	.type	cpt_pop,@object                 # @cpt_pop
	.globl	cpt_pop
	.p2align	3
cpt_pop:
	.quad	0                               # 0x0
	.size	cpt_pop, 8

	.type	my_temp_rax,@object             # @my_temp_rax
	.globl	my_temp_rax
	.p2align	3
my_temp_rax:
	.quad	0                               # 0x0
	.size	my_temp_rax, 8

	.type	my_temp_rcx,@object             # @my_temp_rcx
	.globl	my_temp_rcx
	.p2align	3
my_temp_rcx:
	.quad	0                               # 0x0
	.size	my_temp_rcx, 8

	.type	my_temp_rdx,@object             # @my_temp_rdx
	.globl	my_temp_rdx
	.p2align	3
my_temp_rdx:
	.quad	0                               # 0x0
	.size	my_temp_rdx, 8

	.type	my_temp_rdi,@object             # @my_temp_rdi
	.globl	my_temp_rdi
	.p2align	3
my_temp_rdi:
	.quad	0                               # 0x0
	.size	my_temp_rdi, 8

	.type	my_temp_rsi,@object             # @my_temp_rsi
	.globl	my_temp_rsi
	.p2align	3
my_temp_rsi:
	.quad	0                               # 0x0
	.size	my_temp_rsi, 8

	.type	my_temp_r8,@object              # @my_temp_r8
	.globl	my_temp_r8
	.p2align	3
my_temp_r8:
	.quad	0                               # 0x0
	.size	my_temp_r8, 8

	.type	my_temp_eflags,@object          # @my_temp_eflags
	.globl	my_temp_eflags
	.p2align	3
my_temp_eflags:
	.quad	0                               # 0x0
	.size	my_temp_eflags, 8

	.type	.L.str,@object                  # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Donn\303\251es :\n\tNb Store const =\t%lu\t\tNb Load const =\t%lu\n\tNb Store Total =\t%lu\t\tNb Load Total =\t%lu\nSauvegarde contexte :\n\tNb Push =\t\t%lu\t\tNb Pop =\t%lu\n"
	.size	.L.str, 150

	.ident	"clang version 13.0.0 (git@gricad-gitlab.univ-grenoble-alpes.fr:violetf/llvm-project.git b47387d4949069f80e15343ae2798aa13194f4b4)"
	.section	".note.GNU-stack","",@progbits
	.addrsig
