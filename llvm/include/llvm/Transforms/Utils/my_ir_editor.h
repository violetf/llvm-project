//===-- my_ir_editor.h ------------------------------------------*- C++ -*-===//
// Passe pour ajouter des compteur const/non-const à l'IR
//
//===----------------------------------------------------------------------===//

#ifndef LLVM_TRANSFORMS_UTILS_MY_IR_EDITOR_H
#define LLVM_TRANSFORMS_UTILS_MY_IR_EDITOR_H

#include "llvm/IR/PassManager.h"
#include "llvm/IR/InstrTypes.h"

#include <vector>
#include <string>

// types personnalisés

using ValueConstantsTy		=	std::pair<llvm::Value*, std::vector<int>>;
using VariableConstantsTy	=	std::vector<ValueConstantsTy>;
using FunctionConstantsTy	=	std::vector<VariableConstantsTy>;
using ModuleConstantsTy		=	std::vector<FunctionConstantsTy>;

using FunctionProfileTy		=	std::pair<llvm::Function*, std::vector<std::vector<int>>>;
using ModuleFunProfileTy	=	std::vector<FunctionProfileTy>;


// Crée une copie de la fonction et la renomme
llvm::Function* my_clone_function(llvm::Function*, const std::string&);

// evalue si la valeur donnée est dans la liste, avec depth présent dans les caractéristiques (quelconque par défaut)
bool is_constant(const FunctionConstantsTy&, const llvm::Value*, const int depth = /*-1 pour toutes les valeurs, aka value est dans la liste*/-1);

// renvoie la liste des caractéristiques constantes vector<int>, représentant le degré de const : 0 = const ty, 1 = ty *const, 2 = ty **const, ...
std::vector<int> get_constant_depths(const FunctionConstantsTy&, const llvm::Value*);

// ajoute la valeur à la liste dans le groupe qui contient previous, dans un nouveau sinon. ne fait pas de doublon
void add_constant(FunctionConstantsTy&, llvm::Value*, const std::vector<int>&, const llvm::Value* previous = nullptr);

// propagation récursive des constantes vers le haut déclenché par la détection de l'annotation correspondante
void propag_top(FunctionConstantsTy&, llvm::Value*, const llvm::Value*, const int);

// calcule la profondeur du type. ex : i32 -> 0 | i1* -> 1 | i8** -> 2
int get_depth(const llvm::Type*);

// propagation des constantes vers le bas
void propag_bottom(FunctionConstantsTy&, llvm::Value*);

// prend un nom "nom", une liste de liste de int {{0}, {1,2}, {}, {3}} et renvoie "nom_0_12_nc_3"
// /!\ {1,2} et {12} donnent le même résultat ce qui peut poser problème, mais un pointeur à +de 10 niveau ? (int************ envisageable ?)
// try putting points between same arg caracteristic {{1,2}, {3}} -> nom_1.2._3. ou nom_.1.2_.3
std::string get_modified_name(const std::string&, const std::vector<std::vector<int>>&);

// décode l'annotation et ajoute, selon l'annotation à la liste des constantes globales
void add_global_annotations(VariableConstantsTy&, llvm::Value*, const char*);

// récupère les variables globales constantes
void get_global_annotations(const llvm::Module&, VariableConstantsTy&);

// renvoie la liste_des_args_*_const/non_const
std::vector<std::vector<int>> get_fct_args_qualifiers(const FunctionConstantsTy&, const llvm::CallBase*);

// crée la liste de constantes relative à la fonction courante (doit être fait pour chaque nouvelle modif de fonction, le clone modifie les références d'instruction)
void analyse_constants(llvm::Function*, FunctionConstantsTy&);

// crée un clone de chaque fonction du module
// est appelé en début (après l'initialisation des fct à parcourir/modifier) pour faire une copie saine (sans modification) de chaque fct
void create_clone_database(llvm::Module&, std::map<llvm::Function*, llvm::Function*>&);

// détruit les fonctions clonées
void destroy_clone_database(std::map<llvm::Function*, llvm::Function*>&);

// ajoute une annotation aux Instructions constantes
void add_annotation_on_constants(ModuleConstantsTy&);

namespace llvm {

class MyIREditorPass : public PassInfoMixin<MyIREditorPass> {
public:
	PreservedAnalyses run(Module &M, ModuleAnalysisManager &AM);
	
	static bool isRequired() { return true; }
};

} // namespace llvm

#endif // LLVM_TRANSFORMS_UTILS_MY_IR_EDITOR_H
