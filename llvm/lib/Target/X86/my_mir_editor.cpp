//===- my_mir_editor.cpp -  -----------------------------------------------===//
//
//
//===----------------------------------------------------------------------===//
//
//
//===----------------------------------------------------------------------===//

#include "X86.h"
#include "X86InstrInfo.h"
#include "X86Subtarget.h"
#include "llvm/CodeGen/MachineBasicBlock.h"
#include "llvm/CodeGen/MachineFunction.h"
#include "llvm/CodeGen/MachineFunctionPass.h"
#include "llvm/CodeGen/MachineInstr.h"
#include "llvm/CodeGen/MachineInstrBuilder.h"
#include "llvm/CodeGen/MachineOperand.h"
#include "llvm/IR/DebugLoc.h"
#include "llvm/Support/Debug.h"
#include "llvm/MC/MCContext.h"
#include "llvm/Support/SourceMgr.h"
#include <sstream>

using namespace llvm;

#define DEBUG_TYPE "my_mir_editor"

#define PASS_NAME "X86 add counters"

std::string constAnnotationMetadata = "constAnnotationMetadata";
const std::string ArgName = "ArgConst";

namespace {

class MyMIRPass : public MachineFunctionPass {
public:
	static char ID;

	MyMIRPass() : MachineFunctionPass(ID) {
		initializeMyMIRPassPass(*PassRegistry::getPassRegistry());
	}

	bool runOnMachineFunction(MachineFunction &MF) override;

	StringRef getPassName() const override {
		return PASS_NAME;
	}
};

} // namespace

char MyMIRPass::ID = 0;

INITIALIZE_PASS(MyMIRPass, DEBUG_TYPE, PASS_NAME, false, false)

FunctionPass *llvm::createMyMIRPassPass() { return new MyMIRPass(); }

// calcule la profondeur du type. ex : i32 -> 0 | i1* -> 1 | i8** -> 2
int get_depth(const Type* type) {
	int i = 0;
	if (const PointerType* ptr_ty = dyn_cast<PointerType>(type)) {
		i = 1;
		while((ptr_ty = dyn_cast<PointerType>(ptr_ty->getElementType())))
			i++;
	}
	return i;
}

// return true if it's the correct annotation and modify ret as results
bool get_annot_depths(const char* str, std::vector<int> &ret) {
	std::stringstream ss(str);
	std::vector<std::string> tmp_vect;
	std::string str_buf;
	
	while (std::getline(ss, str_buf, '_'))
		tmp_vect.push_back(str_buf);

	if (tmp_vect.empty() || tmp_vect[0] != constAnnotationMetadata)
		return false;

	for (unsigned i = 1; i < tmp_vect.size(); i++) {
		ret.push_back(std::atoi(tmp_vect[i].c_str()));
	}
	return true;
}

// return true if it's the correct annotation and modify ret as results, made for arguments
bool get_arg_annot_depths(const char* str, std::vector<int> &ret) {
	std::stringstream ss(str);
	std::vector<std::string> tmp_vect;
	std::string str_buf;
	
	while (std::getline(ss, str_buf, '_'))
		tmp_vect.push_back(str_buf);

	if (tmp_vect.size() < 2 || tmp_vect[1] != ArgName)
		return false;

	for (unsigned i = 2; i < tmp_vect.size(); i++) {
		ret.push_back(std::atoi(tmp_vect[i].c_str()));
	}
	return true;
}

// effectue l'affichage des données en sauvegardant le contexte
void add_my_print_cpt(MachineBasicBlock& MBB, MachineInstr* insert_location, DebugLoc& DL, const TargetInstrInfo* TII) {
// save context
	BuildMI(MBB, insert_location, DL, TII->get(X86::MOV64mr)).addReg(X86::RIP).addImm(1).addReg(0).addExternalSymbol("my_temp_rax").addReg(0).addReg(X86::RAX);
	BuildMI(MBB, insert_location, DL, TII->get(X86::LAHF));
	BuildMI(MBB, insert_location, DL, TII->get(X86::MOV64mr)).addReg(X86::RIP).addImm(1).addReg(0).addExternalSymbol("my_temp_eflags").addReg(0).addReg(X86::RAX);

//function body
	BuildMI(MBB, insert_location, DL, TII->get(X86::CALL64pcrel32)).addExternalSymbol("print_cpt");

// reload context
	BuildMI(MBB, insert_location, DL, TII->get(X86::MOV64rm), X86::RAX).addReg(X86::RIP).addImm(1).addReg(0).addExternalSymbol("my_temp_eflags").addReg(0);
	BuildMI(MBB, insert_location, DL, TII->get(X86::SAHF));
	BuildMI(MBB, insert_location, DL, TII->get(X86::MOV64rm), X86::RAX).addReg(X86::RIP).addImm(1).addReg(0).addExternalSymbol("my_temp_rax").addReg(0);
}

cl::opt<bool> EnableVisu("visu", cl::desc(""), cl::init(false), cl::Hidden);


bool MyMIRPass::runOnMachineFunction(MachineFunction &MF) {
	DebugLoc DL;
	const TargetInstrInfo *TII = MF.getSubtarget().getInstrInfo();

	if (EnableVisu) {
		errs() << "\n\033[1;32m" << MF.getName() << "\n\033[1;34m";
	}

	for (auto& MBB : MF) {
		int nb_store_const = 0;
		int nb_load_const = 0;
		int nb_store_tt = 0;
		int nb_load_tt = 0;
		int nb_push = 0;
		int nb_pop = 0;
		MachineInstr* mi = nullptr;

		for (auto& MI : MBB) {

			if (EnableVisu) {
				errs() << MI;
				continue;
			}
			
			if (!MI.memoperands_empty()) {
				bool annot_found = false;
				bool isStore;

				for (auto& it : MI.memoperands()) {
					isStore = it->isStore();
					if (auto* value = it->getValue()) {
						value = value->stripPointerCasts();
						MDNode* MD = nullptr;
						if (const Instruction* inst = dyn_cast<Instruction>(value)) {	// Value::getMetadata est protégé !
							MD = inst->getMetadata(LLVMContext::MD_annotation);
						}
						else if (const GlobalVariable* GV = dyn_cast<GlobalVariable>(value)) {
							MD = GV->getMetadata(LLVMContext::MD_annotation);
						}
						else if (const Argument* arg = dyn_cast<Argument>(value)) {
							std::vector<int> annot_depths;
							if (get_arg_annot_depths(arg->getName().begin(), annot_depths)) {
								annot_found = true;
								int depth = get_depth(value->getType()) - 1; // get_depth return depth of the mem operand, we need the depth-1 for the type itself, not &ty											

								if (annot_depths.empty() || (std::find(annot_depths.begin(), annot_depths.end(), depth) != annot_depths.end())) {
									if (isStore)
										nb_store_const++;
									else
										nb_load_const++;
								}
								else {
									if (isStore)
										nb_store_tt++;
									else
										nb_load_tt++;
								}
							}
							break;
						}

						if (MD) {
							for (auto& op : MD->operands()) {
								if (auto* mdstring = dyn_cast<MDString>(op)) {
									std::vector<int> annot_depths;
									if (get_annot_depths(mdstring->getString().begin(), annot_depths)) {
										annot_found = true;
										int depth = get_depth(value->getType()) - 1; // get_depth return depth of the mem operand, we need the depth-1 for the type itself, not &ty											

										if (annot_depths.empty() || (std::find(annot_depths.begin(), annot_depths.end(), depth) != annot_depths.end())) {
											if (isStore)
												nb_store_const++;
											else
												nb_load_const++;
										}
										else {
											if (isStore)
												nb_store_tt++;
											else
												nb_load_tt++;
										}
										break;
									}
								}
							}
						}
					}
					if (annot_found)
						break;
				}
				if (!annot_found) {
					if (isStore){
						nb_store_tt++;
					}
					else {
						nb_load_tt++;
					}
				}
			}
			unsigned opcode = MI.getOpcode();
			if (opcode == X86::PUSH64r || opcode == X86::PUSH64rmm || opcode == X86::POP64rmm) {
				nb_push++;
			}
			if (opcode == X86::POP64r || opcode == X86::POP64rmm || opcode == X86::PUSH64rmm) {
				nb_pop++;
			}
			else if (MI.isCall()) {
				if (!MI.isReturn())	// isCall && isReturn = TailCall
					nb_push++;		// push adresse de retour

				for (auto it : MI.operands()) {	// operand 0 seems to be the one that hold the wanted data, but no confirmation found
					if (it.isGlobal()) {
						if (it.getGlobal()->getName() == "exit") {
							mi = &MI;
							break;
						}
					}
				}
			}
			else if (MI.isReturn()) {
				nb_pop++;		// pop adresse de retour
				if (MF.getName() == "main") {
					mi = &MI;
				}
			}
		}

		if (nb_store_tt || nb_load_tt || nb_store_const || nb_load_const || nb_push || nb_pop) {
			auto insert_location = MBB.getFirstTerminator();

			BuildMI(MBB, insert_location, DL, TII->get(X86::MOV64mr)).addReg(X86::RIP).addImm(1).addReg(0).addExternalSymbol("my_temp_rax").addReg(0).addReg(X86::RAX);
			BuildMI(MBB, insert_location, DL, TII->get(X86::LAHF));
			if (nb_store_const)
				BuildMI(MBB, insert_location, DL, TII->get(X86::ADD64mi8)).addReg(X86::RIP).addImm(1).addReg(0).addExternalSymbol("cpt_store_const").addReg(0).addImm(nb_store_const);
			if (nb_load_const)
				BuildMI(MBB, insert_location, DL, TII->get(X86::ADD64mi8)).addReg(X86::RIP).addImm(1).addReg(0).addExternalSymbol("cpt_load_const").addReg(0).addImm(nb_load_const);
			if (nb_store_tt)
				BuildMI(MBB, insert_location, DL, TII->get(X86::ADD64mi8)).addReg(X86::RIP).addImm(1).addReg(0).addExternalSymbol("cpt_store_tt").addReg(0).addImm(nb_store_tt);
			if (nb_load_tt)
				BuildMI(MBB, insert_location, DL, TII->get(X86::ADD64mi8)).addReg(X86::RIP).addImm(1).addReg(0).addExternalSymbol("cpt_load_tt").addReg(0).addImm(nb_load_tt);
			if (nb_push)
				BuildMI(MBB, insert_location, DL, TII->get(X86::ADD64mi8)).addReg(X86::RIP).addImm(1).addReg(0).addExternalSymbol("cpt_push").addReg(0).addImm(nb_push);
			if (nb_pop)
				BuildMI(MBB, insert_location, DL, TII->get(X86::ADD64mi8)).addReg(X86::RIP).addImm(1).addReg(0).addExternalSymbol("cpt_pop").addReg(0).addImm(nb_pop);
			BuildMI(MBB, insert_location, DL, TII->get(X86::SAHF));
			BuildMI(MBB, insert_location, DL, TII->get(X86::MOV64rm), X86::RAX).addReg(X86::RIP).addImm(1).addReg(0).addExternalSymbol("my_temp_rax").addReg(0);
		}
		if (mi)
			add_my_print_cpt(MBB, mi, DL, TII);
	}
	return true;
}

