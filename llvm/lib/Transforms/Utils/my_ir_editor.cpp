/* 
my_ir_editor.cpp

Utilise les annotations ajoutées lors de la création de l'IR (llvm.xxx.annotations) afin d'analyser les constantes associées avec la chaîne mise lors de la modification de l'AST.  
Après analyse, modifie l'IR en ajoutant une metadata annotation correspondant au type de constante aux instructions IR afin de transmettre l'information à la génération de code.  
Crée des doublons de fonctions et modifie la référence d'appel pour chaque type de profile, pour pouvoir implanter les bons compteurs.  
*/

#include "llvm/Transforms/Utils/my_ir_editor.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/Transforms/Utils/Cloning.h"
#include "llvm/ADT/APFloat.h"
#include "llvm/Passes/PassBuilder.h"
#include "llvm/IR/MDBuilder.h"

#include <map>
#include <regex>
#include <vector>
#include <string>

using namespace llvm;

// annotations de base
const std::string constAnnotationValue = "constAnnotationValue";
const std::string constAnnotationPointer = "constAnnotationPointer";
const std::string constAnnotationMetadata = "constAnnotationMetadata";
const std::string ArgName = "ArgConst";

// Crée une copie de la fonction et la renomme
Function* my_clone_function(Function* src, const std::string& name) {
	if (src == nullptr) {
		errs() << "Tried to clone a null function.\n";
		exit(1);
	}
	if (name == "") {
		errs() << "Cloned function name must not be empty.\n";
		exit(1);
	}

	ValueMap<const Value*, WeakTrackingVH> ValueMap;
	Function* dest = CloneFunction(src, ValueMap);
	dest->setName(name);

	if (src->hasComdat())
		dest->setComdat(src->getComdat());

	return dest;
}

// evalue si la valeur donnée est dans la liste, avec depth présent dans les caractéristiques (quelconque par défaut)
bool is_constant(const FunctionConstantsTy &constants, const Value* value, const int depth /* = -1*/) {
	for (const auto& list : constants)
	{
		for (const auto& pair : list) {
			if ((pair.first == value) && ((depth == -1) || (std::find(pair.second.begin(), pair.second.end(), depth) != pair.second.end())))
				return true;
		}
	}
	return false;
}

// renvoie la liste des caractéristiques constantes vector<int>, représentant le degré de const : 0 = const ty, 1 = ty *const, 2 = ty **const, ...
std::vector<int> get_constant_depths(const FunctionConstantsTy &constants, const Value* value) {
	if (!value)
		return {};
	
	for (const auto& list : constants)
	{
		for (const auto& pair : list) {
			if (pair.first == value)
				return pair.second;
		}
	}
	return {};
}

// ajoute la valeur à la liste dans le groupe qui contient previous, dans un nouveau sinon. ne fait pas de doublon
void add_constant(FunctionConstantsTy &constants, Value* value, const std::vector<int> &depths, const Value* previous /*=nullptr*/) {
	if (!value)
		return;

	for (auto& list : constants)
	{
		for (auto& pair : list) {
			if (pair.first == value) {
				for (auto& depth : depths) {
					if (std::find(pair.second.begin(), pair.second.end(), depth) == pair.second.end())
						pair.second.push_back(depth);
				}
				std::sort(pair.second.begin(), pair.second.end());
				return;
			}				
		}
	}

	if (previous) {
		for (unsigned i = 0; i < constants.size(); i++)
		{
			for (auto& pair : constants[i]) {
				if (pair.first == previous)
				{
					constants[i].push_back({value, depths});
					std::sort(constants[i].back().second.begin(), constants[i].back().second.end());
					return;
				}
			}
		}
	}
	constants.push_back({std::make_pair(value, depths)});
	std::sort(constants.back().back().second.begin(), constants.back().back().second.end());
}

// propagation récursive des constantes vers le haut déclenché par la détection de l'annotation correspondante
void propag_top(FunctionConstantsTy &constants, Value* value, const Value* previous, const int depth) {
	if (!value)
		return;

	if (Instruction* inst = dyn_cast<Instruction>(value)) {
		switch (inst->getOpcode()) {
			case Instruction::Alloca :
			case Instruction::Call :
				add_constant(constants, inst, {depth}, previous);
				break;
			case Instruction::Load :
				add_constant(constants, inst, {depth}, previous);
				if (inst->getType()->isPointerTy()) {
					propag_top(constants, inst->getOperand(0), inst, depth);
				}
				break;
			case Instruction::BitCast :
				add_constant(constants, inst, {depth}, previous);
				propag_top(constants, inst->getOperand(0), inst, depth);
				break;
			case Instruction::PHI :
				add_constant(constants, inst, {depth}, previous);
				for (auto& it : inst->operands()) {
					if (dyn_cast<Instruction>(it) || dyn_cast<Argument>(it)) {
						propag_top(constants, it, inst, depth);
					}
				}
				break;
			case Instruction::GetElementPtr :
			case Instruction::ExtractElement :
			case Instruction::ExtractValue :
				add_constant(constants, inst, {depth}, previous);
				break;
			default :
				errs() << "Unexpected Instruction : " << *value << "\n";
				break;
		}
	}
	else if (Argument* arg = dyn_cast<Argument>(value))
	{
		add_constant(constants, arg, {depth}, previous);
	}
}

// calcule la profondeur du type. ex : i32 -> 0 | i1* -> 1 | i8** -> 2
int get_depth(const Type* type) {
	int i = 0;
	if (const PointerType* ptr_ty = dyn_cast<PointerType>(type)) {
		i = 1;
		while((ptr_ty = dyn_cast<PointerType>(ptr_ty->getElementType())))
			i++;
	}
	return i;
}

// propagation des constantes vers le bas
void propag_bottom(FunctionConstantsTy &constants, Value* value) {
	if (!value)
		return;

	if (Instruction* inst = dyn_cast<Instruction>(value)) {
		std::vector<int> tmp;
		switch(inst->getOpcode()) {
			case Instruction::Store : // propag store : si ty* ty**
				if (inst->getOperand(0)->getType()->isPointerTy()) {
					if (is_constant(constants, inst->getOperand(0)))
						add_constant(constants, inst->getOperand(1), get_constant_depths(constants, inst->getOperand(0)), inst->getOperand(0));
					else if (is_constant(constants, inst->getOperand(1)))
						add_constant(constants, inst->getOperand(0), get_constant_depths(constants, inst->getOperand(1)), inst->getOperand(1));
				}
				break;
			case Instruction::Load : // propag load : si ty* ty**
				if (!inst->getType()->isPointerTy())
					break;
				[[fallthrough]];
			case Instruction::BitCast :
				if (is_constant(constants, inst->getOperand(0)))
					add_constant(constants, inst, get_constant_depths(constants, inst->getOperand(0)), inst->getOperand(0));
				break;
			case Instruction::GetElementPtr :
			case Instruction::ExtractElement :
			case Instruction::ExtractValue :
				tmp = get_constant_depths(constants, inst->getOperand(0));
				if (std::find(tmp.begin(), tmp.end(), 0) != tmp.end())
					add_constant(constants, inst, {get_depth(inst->getType())-1}, inst->getOperand(0));	// getelemenptr donne un pointeur vers le membre
				break;
			case Instruction::PHI :
				for (auto& it : inst->operands()) {
					if (dyn_cast<Instruction>(it) || dyn_cast<Argument>(it)) {
						if (is_constant(constants, it)) {
							add_constant(constants, inst, get_constant_depths(constants, it), it);
							break;
						}
					}
				}
				break;
		}
	}
}

// prend un nom "nom", une liste de liste de int {{0}, {1,2}, {}, {3}} et renvoie "nom_0_12_nc_3"
// /!\ {1,2} et {12} donnent le même résultat ce qui peut poser problème, mais un pointeur à +de 10 niveau ? (int************ envisageable ?)
// try putting points between same arg caracteristic {{1,2}, {3}} -> nom_1.2._3. ou nom_.1.2_.3
std::string get_modified_name(const std::string &old_name, const std::vector<std::vector<int>> &vect) {
	std::string new_name = old_name;
	for (auto& v : vect) {
		new_name += '_';
		if (v.empty())
			new_name += "nc";
		for (auto i : v)
			new_name += std::to_string(i)+'.';
	}
	return new_name;
}

// décode l'annotation et ajoute, selon l'annotation à la liste des constantes globales
void add_global_annotations(VariableConstantsTy &global_constants, Value* AnnotatedGlobal, const char* annot) {
	int annot_int;
	if (annot == constAnnotationValue)
		annot_int = 0;
	else if (std::regex_match(annot, std::regex(constAnnotationPointer + "[0-9]+")))
		annot_int = std::atoi(annot+constAnnotationPointer.length());
	else
		return;

	for (auto& pair : global_constants) {
		if (pair.first == AnnotatedGlobal) {
			if (std::find(pair.second.begin(), pair.second.end(), annot_int) == pair.second.end())
				pair.second.push_back(annot_int);
			return;
		}
	}
	global_constants.push_back({AnnotatedGlobal, {annot_int}});
}

// récupère les variables globales constantes
// original code from https://gist.github.com/serge-sans-paille/c57d0791c7a9dbfc76e5c2f794e650b4
void get_global_annotations(Module &M, VariableConstantsTy &global_constants) {
	if(GlobalVariable* GA = M.getGlobalVariable("llvm.global.annotations")) {
		// the first operand holds the metadata
		for (Value *AOp : GA->operands()) {
			// all metadata are stored in an array of struct of metadata
			if (ConstantArray *CA = dyn_cast<ConstantArray>(AOp)) {
				// so iterate over the operands
				for (Value *CAOp : CA->operands()) {
					// get the struct, which holds a pointer to the annotated function
					// as first field, and the annotation as second field
					if (ConstantStruct *CS = dyn_cast<ConstantStruct>(CAOp)) {
						if (CS->getNumOperands() >= 2) {
							Value* AnnotatedGlobal = CS->getOperand(0)->getOperand(0);
							/* if (dyn_cast<Function>(AnnotatedGlobal))
								continue;	// ignore annotated Functions 
							// Deprecated since annotation in AST */
							// the second field is a pointer to a global constant Array that holds the string
							if (GlobalVariable *GAnn = dyn_cast<GlobalVariable>(CS->getOperand(1)->getOperand(0))) {
								if (ConstantDataArray *A = dyn_cast<ConstantDataArray>(GAnn->getOperand(0))) {
									// we have the annotation!
									StringRef AS = A->getAsString();
									add_global_annotations(global_constants, AnnotatedGlobal, AS.begin());											
								}
							}
						}
					}
				}
			}
		}
	}
	// les globales ajoutées dans l'IR qui n'apparaissent pas directement dans l'AST (ex : les données brutes d'un tableau)
	// comme il n'y a plus les types source, on ne peut pas chercher en détaille les niveaux de constante (ex const int* const)
	// ces données sont principalement utilisées avec des memcpy, on ne verra pas l'effet quand le memcpy n'est pas inliné tant que les lib ne seront pas intégrées
	for (auto it = M.global_begin(); it != M.global_end(); it++) {
		auto GV = dyn_cast<GlobalVariable>(it);
		if (GV && GV->isConstant() && !is_constant({global_constants}, GV)) {	
			MDBuilder MDB(GV->getContext());
			SmallVector<Metadata *, 4> Names;
			Names.push_back(MDB.createString(constAnnotationMetadata));
			
			MDNode *MD = MDTuple::get(GV->getContext(), Names);
			GV->setMetadata(LLVMContext::MD_annotation, MD);
		}
	}
}

// renvoie la liste_des_args_*_const/non_const
std::vector<std::vector<int>> get_fct_args_qualifiers(const FunctionConstantsTy &constants, const CallBase* call) {
	std::vector<std::vector<int>> tmp = {};
	for (const Use& U : call->args()) {
		const Value* arg = U.get();
		if (arg->getType()->isPointerTy())
			tmp.push_back(get_constant_depths(constants, arg));
	}
	return tmp;
}

// crée la liste de constantes relative à la fonction courante (doit être fait pour chaque nouvelle modif de fonction, le clone modifie les références d'instruction)
void analyse_constants(Function* F, FunctionConstantsTy &constants) {
	if (!F)
		return;

	for (auto& BB : *F)
	{
		for (auto& I : BB)
		{
			if (CallInst* call = dyn_cast<CallInst>(&I))
			{
				Function* calledFunc;		
				if ((calledFunc = dyn_cast<Function>(call->getCalledOperand()->stripPointerCasts())) && (std::regex_match(calledFunc->getName().begin(), std::regex("llvm\\.var\\.annotation|llvm\\.ptr\\.annotation\\.p.*"))))
				{
					const char* annot = dyn_cast<ConstantDataArray>(dyn_cast<GlobalValue>(dyn_cast<Constant>(call->getOperand(1))->getOperand(0))->getOperand(0))->getAsString().begin();
					if (annot == constAnnotationValue)
					{
						add_constant(constants, call, {0});
						propag_top(constants, call->getOperand(0), call, 0);
					}
					else if (std::regex_match(annot, std::regex(constAnnotationPointer + "[0-9]+"))) {
						int annot_int = std::atoi(annot+constAnnotationPointer.length());
						add_constant(constants, call, {annot_int});
						propag_top(constants, call->getOperand(0), call, annot_int);
					}
				}
			}
			// propag_bottom(constants, &I); // pour une analyse complète des constants en une fonction, mais à cause des modif d'ajout arg const, ne peut pas ici
		}
	}
}

// crée un clone de chaque fonction du module
// est appelé en début (après l'initialisation des fct à parcourir/modifier) pour faire une copie saine (sans modification) de chaque fct
void create_clone_database(Module &M, std::map<Function*, Function*> &clone_database) {
	std::vector<Function*> tmp_vect;

	for (auto& F : M)
		tmp_vect.push_back(&F);

	for (auto it : tmp_vect)
		clone_database[it] = my_clone_function(it, std::string(it->getName())+"_");
}

// détruit les fonctions clonées
void destroy_clone_database(std::map<Function*, Function*> &clone_database) {
	for (auto& it : clone_database) {
		it.second->eraseFromParent();
	}
}

// ajoute une annotation aux Instructions constantes
void add_annotation_on_constants(ModuleConstantsTy &Module_constants) {
	for (auto& Function_constants : Module_constants) {
		for (auto& Var_constants : Function_constants) {
			for (auto pair : Var_constants) {
				if (Instruction* inst = dyn_cast<Instruction>(pair.first)) {
					std::string tmp;
					for (auto i : pair.second)
						tmp += '_' + std::to_string(i);
					inst->addAnnotationMetadata(constAnnotationMetadata+tmp);
				}
				else if (Argument* arg = dyn_cast<Argument>(pair.first)) {
					std::string tmp;
					for (auto i : pair.second)
						tmp += '_' + std::to_string(i);
					arg->setName(std::to_string(arg->getArgNo())+'_'+ArgName+tmp);
				}
			}
		}
	}
}

PreservedAnalyses MyIREditorPass::run(Module &M, ModuleAnalysisManager &AM) {

// Fonctions
 	Function* main_fct = M.getFunction("main");

// variables
	VariableConstantsTy global_constants;
	std::map<FunctionProfileTy, Function*> fct_ref_map;
	ModuleFunProfileTy fct_a_ecrire;
	std::map<Function*, Function*> clone_database; // stocke une copie saine (sans modification) de chaque fct
	ModuleConstantsTy all_constants;

// récupère la liste des variables globales constantes
	get_global_annotations(M, global_constants);


// parcours l'ensemble des fonctions avec une analyse des const préalable, et de proche en proche ajoute les nouveaux profiles const aux fct à parcourir

	// on ajoute toutes les fonctions à la liste avec {} pour qu'elles aient une version identique à leur profile 
	// permet dans le cas d'appels indirects vers une fonction qui n'est pas appelée autrement d'avoir mis les annotations de constantes
	for (auto& F : M) {
		if (&F != main_fct) {
			fct_ref_map[{&F, {}}] = &F;
			fct_a_ecrire.push_back({&F, {}});
		}
	}
	// on souhaite parcourir le main en premier (je ne pense pas qu'il y ai encore de vraie raison à cela, maintenant que toutes les fct de base auront un parcours)
	fct_ref_map[{main_fct, {}}] = main_fct;
	fct_a_ecrire.push_back({main_fct, {}});

	// crée la clone_database
	create_clone_database(M, clone_database);
	
	while(!fct_a_ecrire.empty())
	{
		Function* current_writing_fct = fct_a_ecrire.back().first;
		std::vector<std::vector<int>> current_qualifiers = fct_a_ecrire.back().second;
		FunctionConstantsTy current_constants = {global_constants};
		fct_a_ecrire.pop_back();

		if (!current_writing_fct) {
			errs() << "Fonction non-existante !\n";
			continue;
		}

		analyse_constants(current_writing_fct, current_constants);

		if (!current_qualifiers.empty()) {	// on n'affecte pas les args si on impose {}
			int i = 0;	// comme la liste est construite, en faisant évoluer i en même temps que l'on trouve un arg ty*, arg et qualif[i] correspondent au même argument
			for (auto& arg : current_writing_fct->args()) {
				if (arg.getType()->isPointerTy()) {
					if (!current_qualifiers[i].empty())
						add_constant(current_constants, &arg, current_qualifiers[i]);
					i++;
				}
			}
		}
			
		Function* called_fct;
		std::vector<std::vector<int>> called_qualifiers;
		Function* new_fct;
		// itération sur instr : propag_bottom + detect callbase (name != llvm.[...])
		for (auto& BB : *current_writing_fct) {
			for (BasicBlock::iterator inst = BB.begin(); inst != BB.end(); inst++) {

				propag_bottom(current_constants, dyn_cast<Instruction>(inst)); // doit se faire ici, dans le cas d'ajout d'arg comme constante ne peut pas se faire avant

				if (CallBase* call = dyn_cast<CallBase>(inst)) {
					if ((called_fct = dyn_cast<Function>(call->getCalledOperand()->stripPointerCasts())) && (!std::regex_match(called_fct->getName().begin(), std::regex("llvm\\..*"))))
					{
						if (!called_fct->isDeclaration()) // on ne va pas écrire une fonction qui n'est pas définie dans ce fichier
						{
							called_qualifiers = get_fct_args_qualifiers(current_constants, call);

							if (called_qualifiers.empty())
								continue;	// called_qualifiers == {} signifie qu'il n'y a pas de ptr vers const possible, pas de changement de fct à faire

							if (fct_ref_map.find({called_fct, called_qualifiers}) == fct_ref_map.end())	// on ajoute une fonction pas encore vue à la liste des fcts "vues" et "à écrire"
							{
							// on crée la nouvelle fct
								new_fct = my_clone_function(clone_database.at(called_fct), get_modified_name(called_fct->getName().begin(), called_qualifiers));
								call->setCalledFunction(new_fct);
							// on l'ajoute à la liste des fcts
								fct_ref_map[{called_fct, called_qualifiers}] = new_fct;
								fct_a_ecrire.push_back({new_fct, called_qualifiers});
							}
							else
								call->setCalledFunction(fct_ref_map[{called_fct, called_qualifiers}]);
						}
					}
				}
			}
		}

		all_constants.push_back(current_constants);
	}
	destroy_clone_database(clone_database);

	add_annotation_on_constants(all_constants);

	return PreservedAnalyses::none();
}

